<?php   
    function getConnection(){
        $server = "localhost";
        $user = "root";
        $password = "";
        $database = "crud";
        $connection = mysqli_connect($server, $user, $password, $database);
        return $connection;        
    }

    function getProduct($id){
        $consult_products = "SELECT * FROM products WHERE id = " . $id;
        $connection = getConnection();
        $product = mysqli_query($connection, $consult_products);
        $prod = array();
        
        while ($row = mysqli_fetch_assoc($product)) {
            $prod['name'] = $row['name'];
            $prod['price'] = $row['price'];
        } 
        mysqli_free_result($product);
        mysqli_close($connection);
        
        return $prod;
    }
    if (isset($_GET["id"])){
        $id = $_GET["id"];       
        $prod = getProduct($id);
    }

    if (isset($_POST["submit"])){    
        $name = $_POST["name"];
        $price = $_POST["price"];
        $id = $_POST["id"];
        if (!$name or !$price){
            die("Preencha corretamente");
        }
        
        $update_products = "UPDATE products SET ";
        $update_products .= "name = '" . $name . "', ";
        $update_products .= "price = " . $price;
        $update_products .= " WHERE id = " . $id . ";";
        
        $connection = getConnection();
        if (mysqli_query($connection, $update_products)) {
            echo "Produto atualizado com sucesso";
        } else {
            echo "Error: " . $insert_products . "<br>" . mysqli_error($connection);
        }
        $prod = getProduct($id);
    }

?>
<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">        
        <meta http-equiv="X-UA-Compatible" content="IE-edge">
        <meta name="viewport" content="width=devide-width, initial-scale=1">
        <title>CRUD</title>
        
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">CRUD</a>
                </div>
            </div>
        </nav>
        <div id="main" class="container-fluid">
            <h3 class="page-header">Alterar Produto</h3>
            <form action="edit.php" method="post">
                <div class="row">
                    <input id="id" name="id" type="hidden" value="<?php echo $id; ?>"/>
                    <div class="form-group col-md-6">
                        <label for="name">Nome do produto:</label>
                        <input type="text" class="form-control" id="name"  name="name" value = "<?php echo $prod["name"]; ?>">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="price">Preço do produto:</label>
                        <input type="text" class="form-control" id="price" name="price" value = "<?php echo $prod["price"]; ?>">
                    </div>
                </div>
                <hr />
                <div id="actions" class="row">
                    <div class="col-md-12">
                        <input type="submit" name="submit" class="btn btn-primary btn-send" value="Alterar">
                        <a href="index.php" class="btn btn-default">Voltar</a>
                    </div>
                </div>
            </form>
        </div>     
    </body>
</html>