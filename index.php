<?php   
    function getConnection(){
        $server = "localhost";
        $user = "root";
        $password = "";
        $database = "crud";
        $connection = mysqli_connect($server, $user, $password, $database);
        return $connection;        
    }
    $connection = getConnection();

    $consult_products = "SELECT * FROM products";
    $products = mysqli_query($connection, $consult_products);

    if (!$products){
        die("Falhou");
    }

?>
<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">        
        <meta http-equiv="X-UA-Compatible" content="IE-edge">
        <meta name="viewport" content="width=devide-width, initial-scale=1">
        <title>CRUD</title>
        
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">CRUD</a>
                </div>
            </div>
        </nav>
        
        <div id="main" class="container-fluid">
            
                <div id="top" class="row">
                    <div class="col-md-6">
                        <h2>Produtos</h2>
                    </div> 
                    <div class="col-md-6">
                        <a href="add.php" class="btn btn-primary pull-right h2">Cadastrar Produto</a>
                    </div>
                </div> 

                <hr />
                <div id="list" class="row">
                    <div class="table-responsive col-md-12">
                        <table class="table table-striped" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nome</th>
                                    <th>Preço</th>
                                    <th class="actions">Ações</th>
                                 </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    while($prod = mysqli_fetch_assoc($products)){
                                        echo "                    
                                        <tr>
                                            <td>" . $prod["id"] . "</td>
                                            <td>" . $prod["name"] . "</td>
                                            <td>" . $prod["price"] . "</td>";
                                ?>
                                <td class="actions">
                                    <a class="btn btn-success btn-xs" href="view.php?id=<?php echo $prod["id"]; ?>">Visualizar</a>
                                    <a class="btn btn-warning btn-xs" href="edit.php?id=<?php echo $prod["id"]; ?>">Editar</a>
                                </td>
                                <?php 
                                        echo "</tr>";
                                    }
                                ?>
                            </tbody>
                         </table>
                     </div>
                </div>    
        </div>        
    </body>
</html>
<?php
    mysqli_free_result($products);
    mysqli_close($connection);
?>
