<?php   
    function getConnection(){
        $server = "localhost";
        $user = "root";
        $password = "";
        $database = "crud";
        $connection = mysqli_connect($server, $user, $password, $database);
        return $connection;        
    }

    function getProduct($id){
        $consult_products = "SELECT * FROM products WHERE id = " . $id;
        $connection = getConnection();
        $product = mysqli_query($connection, $consult_products);
        $prod = array();
        
        while ($row = mysqli_fetch_assoc($product)) {
            $prod['name'] = $row['name'];
            $prod['price'] = $row['price'];
        } 
        mysqli_free_result($product);
        mysqli_close($connection);
        
        return $prod;
    }
    if (isset($_GET["id"])){
        $id = $_GET["id"];       
        $prod = getProduct($id);
    } 

    if (isset($_GET["action"])){
        $action = $_GET["action"];       
        $delete_products = "DELETE FROM products WHERE id = " . $id;
        $connection = getConnection();
        
        if (mysqli_query($connection, $delete_products)) {
            header("Location:index.php");
        } else {
            echo "Erro deletando produto: " . mysqli_error($connection);
        }
        
        mysqli_close($connection);
    }

?>
<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">        
        <meta http-equiv="X-UA-Compatible" content="IE-edge">
        <meta name="viewport" content="width=devide-width, initial-scale=1">
        <title>CRUD</title>
        
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        
        <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modalLabel">Excluir Produto</h4>
                    </div>
                    <div class="modal-body">
                        Deseja realmente excluir este produto?
                    </div>
                    <div class="modal-footer" id="actions" method="post">
                        <a href="view.php?action=delete&id=<?php echo $id; ?>" class="btn btn-primary btn-send">Sim</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
                    </div>
                </div>
            </div>
        </div>        
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">CRUD</a>
                </div>
            </div>
        </nav>
        
        <div id="main" class="container-fluid">
            <h3 class="page-header">Informações do produto #<?php echo $id; ?></h3>
            <div class="row">
                <div class="col-md-6">
                    <p><strong>Nome do produto</strong></p>
                    <p><?php echo $prod["name"];  ?></p>
                </div>
                <div class="col-md-6">
                    <p><strong>Preço do produto</strong></p>
                    <p><?php echo $prod["price"];  ?></p>
                </div> 
            </div>
            <hr />
            <div id="actions" class="row">
                <div class="col-md-12">
                    <a href="edit.php?id=<?php echo $id; ?>" class="btn btn-primary">Editar</a>
                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#delete-modal">Excluir</a>
                    <a href="index.php" class="btn btn-default">Voltar</a>
                </div>
            </div>
        </div>       
    
    </body>
</html>