<?php   
    $server = "localhost";
    $user = "root";
    $password = "";
    $database = "crud";
    $connection = mysqli_connect($server, $user, $password, $database);

    $insert_products = "INSERT INTO products (name, price) VALUES (";
    if (isset($_POST["submit"])){
        $name = $_POST["name"];
        $price = $_POST["price"];
        if (!$name or !$price){
            die("Preencha corretamente");
        }
        
        $insert_products .= "'" . $name . "', ";
        $insert_products .= $price;
        $insert_products .= ");";
            
        if (mysqli_query($connection, $insert_products)) {
            echo "Produto cadastrado com sucesso";
        } else {
            echo "Erro: " . $insert_products . "<br>" . mysqli_error($connection);
        }
    }

?>
<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">        
        <meta http-equiv="X-UA-Compatible" content="IE-edge">
        <meta name="viewport" content="width=devide-width, initial-scale=1">
        <title>CRUD</title>
        
        <link href="css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.php">CRUD</a>
                </div>
            </div>
        </nav>
        
        <div id="main" class="container-fluid">
            <h3 class="page-header">Cadastrar Produto</h3>
            <form action="add.php" method="post">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="name">Nome do produto:</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="price">Preço do produto:</label>
                        <input type="text" class="form-control" id="price" name="price">
                    </div>
                </div>
                <hr />
                <div id="actions" class="row">
                    <div class="col-md-12">
                        <input type="submit" name="submit" class="btn btn-primary btn-send" value="Cadastrar">
                        <a href="index.php" class="btn btn-default">Voltar</a>
                    </div>
                </div>
            </form>
        </div> 
    </body>
</html>
<?php
    mysqli_close($connection);
?>